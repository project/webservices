<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Simple and direct User ID authentication.
 */

class _webservices_directuid_auth {
  /**
   * Change the argument list for services.
   *
   * @param &$methods
   *   Array. The complete list of all methods, declared using
   *   hook_webservices_info().
   * @param &$key
   *   String. The current method name, like 'node.get' or 'user.save'.
   */
  function fields(&$methods, &$key) {
    // On ALL services, the consumer must provide the User ID
    array_unshift($methods[$key]['#args'],
      array(
        '#name'           => 'consumer_uid',
        '#type'           => 'int',
        '#description'    => t('Service consumer User ID.'),
      )
    );
  }

  /**
   * Authentication method responsible for:
   *   - Check if the user and the agent (the external application) are
   *     who they are saying they are
   *   - Load the real user, which is gener
   *
   * @param &$method
   *   Array. The method information, like the one declared using
   *   hook_webservices_info(), plus any other modification.
   * @param &$args
   *   Array. The received list of arguments from the external application.
   */
  function load(&$method, &$args) {
    // Get the first argument as the User ID and
    // remove it from the arguments list
    $uid = array_shift($args);

    if (!is_numeric($uid) or !$user_temp = user_load($uid)) {
      throw new Exception(t('Invalid user ID.'));
    }

    global $user;
    $user = $user_temp;
  }
}
