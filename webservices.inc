<?php
/**
 * @author Web Services Dev Team
 * @file
 * All functions that are not hooks or constantly accessed.
 */

/**
 * Callback for crossdomain.xml
 */
function _webservices_crossdomain_xml() {
  $output = '<?xml version="1.0"?>'."\n".
    '<!DOCTYPE cross-domain-policy SYSTEM "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">'."\n".
    '<cross-domain-policy>'."\n".
    '  <allow-access-from domain="'. check_plain($_SERVER['HTTP_HOST']) .'" />'."\n".
    '  <allow-access-from domain="*.'. check_plain($_SERVER['HTTP_HOST']) .'" />'."\n".
    '</cross-domain-policy>';

  // Include the XML header before the content
  header('Connection: close');
  header('Content-Length: '. drupal_strlen($output));
  header('Content-Type: text/xml');
  header('Date: '. date('r'));

  // Print the XML
  echo $output;
  exit;
}

/**
 * Prepare an error message for returning to the XMLRPC caller.
 *
 * @param $message
 *   String. The error message.
 * @return
 *   String. Depends on the services server.
 */
function webservices_error($message) {
  // Restore the user data
  global $user, $user_backup;
  $user = $user_backup;

  $server_info = _webservices_server_get();

  // Look for custom error handling function.
  // Should be defined in each server module.
  if ($server_info and module_hook($server_info->module, 'server_error')) {
    return module_invoke($server_info->module, 'server_error', $message);
  }

  // No custom error handling function found.
  return $message;
}

/**
 * Callback for server endpoint
 */
function _webservices_server($server_path = NULL) {
  // Find which module the server is part of
  foreach (module_implements('webservices_server_info') as $module) {
    $info = module_invoke($module, 'webservices_server_info');
    if ($info['#path'] == $server_path) {

      // Call the server
      $server_info = new stdClass();
      $server_info->module = $module;
      $server_info->drupal_path = getcwd();
      _webservices_server_get($server_info);
      print module_invoke($module, 'server');

      // Do not let this output
      module_invoke_all('exit');
      exit;
    }
  }
  // return 404 if the service doesn't exist
  drupal_not_found();
}

/**
 * Get the into from the current server. If none was yet set,
 * then set the server given as current.
 *
 * @param $server_info
 *   Object (optional). The current server info.
 * @return
 *   Object. The current server info. Or FALSE if none was set
 *   and none was given.
 */
function _webservices_server_get($server_info = NULL) {
  static $info;
  if (empty($info)) {
    if (!empty($server_info)) {
      $info = $server_info;
    }
    else {
      return FALSE;
    }
  }
  return $info;
}

/**
 * Execute a given service.
 */
function _webservices_service_call($method_name, $args = array()) {
  // Check that method exists.
  if (!$method = _webservices_service_get($method_name)) {
    return webservices_error(t('Method %name does not exist.', array('%name' => $method_name)));
  }

  // Check for missing args
  foreach ($method['#args'] as $key => $arg) {
    if (empty($arg['#optional']) and !isset($args[$key])) {
      return webservices_error(t('Missing required arguments.'));
    }
  }

  // Authentication methods are responsible for:
  //   - Check if the user and the agent (the external application) are
  //     who they are saying they are
  //   - Load the real user, which is generally not the one who accessed
  //     the site (external application)
  global $user, $user_backup;
  session_save_session(FALSE);
  $user_backup = $user;
  if (empty($method['#no_auth'])) {
    $authentication = module_invoke(variable_get('webservices_auth', 'webservices'), 'webservices_auth_info');
    if (!empty($authentication['#file'])) {
      module_load_include($authentication['#file']['file'], $authentication['#file']['module']);
    }
    $auth = new $authentication['#class'];
    try {
      $auth->load($method, $args);
    }
    catch (Exception $e) {
      return webservices_error($e->getMessage());
    }
  }

  // Load the file where the service callback is
  if ($file = $method['#file']) {
    module_load_include($file['file'], $file['module']);
  }

  // Check access
  $access_arguments = isset($method['#access arguments']) ? $method['#access arguments'] : $args;
  // Call default or custom access callback
  if (!empty($method['#access callback'])
      and $method['#access callback'] != TRUE
      and function_exists($method['#access callback'])
      and call_user_func_array($method['#access callback'], $access_arguments) != TRUE) {
    return webservices_error($method['#access callback']);
    return webservices_error(t('Access denied.'));
  }

  // Change working directory to drupal root to call drupal function,
  // then change it back to server module root to handle return.
  $server_root = getcwd();
  $server_info = _webservices_server_get();
  if ($server_info) {
    chdir($server_info->drupal_path);
  }
  $result = call_user_func_array($method['#callback'], $args);
  if ($server_info) {
    chdir($server_root);
  }

  // Restore the user data
  $user = $user_backup;
  session_save_session(TRUE);

  return $result;
}

/**
 * Get the into from a service.
 *
 * @param $method_name
 *   String. The service name
 * @return
 *   Object. The service info.
 */
function _webservices_service_get($method_name) {
  static $method_cache;
  if (!isset($method_cache[$method_name])) {
    foreach (webservices_service_get_all() as $method) {
      if ($method_name == $method['#method']) {
        $method_cache[$method_name] = $method;
        break;
      }
    }
  }
  return $method_cache[$method_name];
}

/**
 * Build a complete list of all services available,
 *
 * @note This should probably be cached in drupal cache.
 */
function webservices_service_get_all() {
  static $methods_cache;
  if (!isset($methods_cache)) {
    $methods = module_invoke_all('webservices_info');
    foreach ($methods as $key => $method) {
      // Add the access functions of each service
      if (!isset($methods[$key]['#access callback'])) {
        $methods[$key]['#access callback'] = 'user_access';
        if (!isset($methods[$key]['#access arguments'])) {
          $methods[$key]['#access arguments'] = array('access webservices');
        }
      }

      if (!isset($methods[$key]['#args'])) {
        $methods[$key]['#args'] = array();
      }

      // Let the authentication method change the normal set of arguments
      $authentication = module_invoke(variable_get('webservices_auth', 'webservices'), 'webservices_auth_info');
      if (!empty($authentication['#file'])) {
        module_load_include($authentication['#file']['file'], $authentication['#file']['module']);
      }
      $auth = new $authentication['#class'];
      $auth->fields($methods, $key);

      // Set defaults for args
      foreach ($methods[$key]['#args'] as $arg_key => $arg) {
        if (is_array($arg)) {
          if (!isset($arg['#optional'])) {
            $methods[$key]['#args'][$arg_key]['#optional'] = FALSE;
          }
        }
        else {
          $arr_arg = array();
          $arr_arg['#name'] = t('unnamed');
          $arr_arg['#type'] = $arg;
          $arr_arg['#description'] = t('No description given.');
          $arr_arg['#optional'] = FALSE;
          $methods[$key]['#args'][$arg_key] = $arr_arg;
        }
      }
      reset($methods[$key]['#args']);
    }
    $methods_cache = $methods;
  }
  return $methods_cache;
}
