<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Link general system functionalities to services module.
 */

function wsservice_dcore_system_mail($mailkey, &$message, $params) {
  $language = $message['language'];
  $variables = user_mail_tokens($params['account'], $language);
  if (function_exists('locale') and $language->language != 'en') {
    $params['subject'] =locale($params['subject'], $language->language);
    $params['body'] = locale($params['body'], $language->language);
  }
  $message['subject'] = strtr($params['subject'], $variables);
  $message['body'] = strtr($params['body'], $variables);
  foreach ($params['headers'] as $header => $val) {
    $message['headers'][$header] = $val;
  }
}

/**
 * Send an email using the Services module.
 */
function wsservice_dcore_system_mailprepare($mailkey, $to, $subject, $body, $from = NULL, $headers = array()) {
  $params = array();
  $params['subject'] = $subject;
  $params['body'] = $body;
  $params['headers'] = $headers;
  $status = drupal_mail('wsservice_dcore', $mailkey, $to, user_preferred_language($to), $params, $from, TRUE);

  if (!$status) {
    return webservices_error(t('There was a problem sending your email.'));
  }
  return $status;
}

/**
 * Returns a specified variable.
 */
function wsservice_dcore_system_getvariable($name, $default = NULL) {
  return variable_get($name, $default);
}

/**
 * Set a variable.
 */
function wsservice_dcore_system_setvariable($name, $value) {
  variable_set($name, $value);
}

/**
 * Check if a module is enabled. If so, return its version.
 */
function wsservice_dcore_system_module_exists($module) {
  if (module_exists($module)) {
    $modules = module_rebuild_cache();
    if (array_key_exists($module, $modules)) {
      return (string)$modules[$module]->info['version'];
    }
  }

  return '';
}
