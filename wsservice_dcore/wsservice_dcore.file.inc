<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Link general file functionalities to services module.
 */

/**
 * Get all elements fomr a given file
 *
 * @param $fid
 *   Number. File ID
 * @return
 *   Array. All elements fomr a given file
 */
function _wsservice_dcore_file_get($fid) {
  if ($file = db_fetch_array(db_query('SELECT * FROM {files} WHERE fid = %d', $fid))) {
    $filepath = file_create_path($file->filepath);
    $binaryfile = fopen($filepath, 'rb');
    $file['file'] = base64_encode(fread($binaryfile, filesize($filepath)));
    fclose($binaryfile);
    return $file;
  }
  else {
    return webservices_error(t('There is no file with the given ID.'));
  }
}

/**
 * Check if the user has permission to get a given file
 */
function _wsservice_dcore_file_get_access($fid) {
  global $user;
  if (user_access('get any binary files')) {
    return TRUE;
  }
  elseif ($file = db_fetch_array(db_query('SELECT * FROM {files} WHERE fid = %d', $fid))) {
    return $file['uid'] == $user->uid and user_access('get own binary files');
  }
}

/**
 * Generates an array of base64 encoded files attached to a node
 *
 * @param $nid
 *   Number. Node ID
 * @return
 *   Array. A list of all files from the given node
 */
function _wsservice_dcore_file_get_node_files($nid) {
  $node = node_load($nid);
  if (isset($node->files)) {
    $files = array();
    foreach ($node->files as $file) {
      // Rebuild the files array so it only contains files we know we're allowed to list
      if ($file->list) {
        $files[] = $file;
      }
    }
    if (count($files) > 0) {
      $send = array();
      foreach ($files as $file) {
        $file = array_shift($files);
        $filepath = file_create_path($file->filepath);
        $binaryfile = fopen($filepath, 'rb');
        $send[$file->fid] = array(
          'file'      => base64_encode(fread($binaryfile, filesize($filepath))),
          'filename'  => $file->filename,
          'uid'       => $file->uid,
          'filemime'  => $file->filemime,
          'filesize'  => $file->filesize,
          'status'    => $file->status,
          'timestamp' => $file->timestamp
        );
        fclose($binaryfile);
      }
    }
    return $send;
  }
  else {
    return webservices_error(t('There are no files on given node.'));
  }
}

/**
 * Check if the user has permission to get all files from a given node.
 */
function _wsservice_dcore_file_get_node_files_access($nid) {
  global $user;
  if (user_access('get any binary files')) {
    return TRUE;
  }
  elseif ($node = node_load($nid)) {
    return $node->uid == $user->uid and user_access('get own binary files');
  }
}

/**
 * Insert a new file into the system.
 *
 * @param $filename
 *   String. The file name, including extension.
 * @param $data
 *   Data. File content.
 * @param $path
 *   String (optional). Path in which the file will be saved.
 * @param $replace
 *   Number (optional). In case the file exists, what is the behaviour. It can be:
 *    - 0 FILE_EXISTS_RENAME (default) - Append _{incrementing number} until the filename is unique
 *    - 1 FILE_EXISTS_REPLACE - Replace the existing file
 *    - 2 FILE_EXISTS_ERROR - Do nothing and return FALSE.
 * @param $status
 *   Number (optional). If the file is temporary or permanent (default).
 * @return
 *   Object. The file object.
 */
function _wsservice_dcore_file_insert($filename, $data, $path = '', $replace = FILE_EXISTS_RENAME, $status = FILE_STATUS_PERMANENT) {
  // Check if the path exists
  if (empty($path)) {
    $path = file_directory_path();
  }
  $dest = $path .'/'. $filename;

  //Try to save the file
  if (!$file->filepath = file_save_data($data, $dest, $replace)) {
    return webservices_error(t('File could not be saved.'));
  }

  // If we made it this far it's safe to record this file in the database.
  $file->uid = $user->uid;
  $file->status = $status;
  $file->timestamp = time();
  drupal_write_record('files', $file);

  // Re
  return $file;
}
