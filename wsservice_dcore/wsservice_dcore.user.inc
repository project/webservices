<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Link general user functionalities to services module.
 */

/**
 * Delete an user.
 *
 * @param $uid
 *   Number. The user ID.
 */
function _wsservice_dcore_user_delete($uid) {
  $account = user_load($uid);
  if (empty($account)) {
    return webservices_error(t('There is no user with such ID.'));
  }
  user_delete($account, $uid);

  // Everything went right.
  return TRUE;
}

/**
 * Check if the user is allowed to delete the user.
 *
 * @param $uid
 *   Number. The user ID.
 */
function _wsservice_dcore_user_delete_access($uid) {
  global $user;
  return (($user->uid == $uid and user_access('delete own user')) or
    ($user->uid != $uid and user_access('delete any user')));
}

/**
 * Get user details.
 *
 * @param $uid
 *   Number. The user ID.
 */
function _wsservice_dcore_user_get($uid) {
  $account = user_load($uid);
  if (empty($account)) {
    return webservices_error(t('There is no user with such ID.'));
  }

  // Everything went right.
  return $account;
}

/**
 * Check if the user is allowed to get the user data.
 *
 * @param $uid
 *   Number. The user ID.
 */
function _wsservice_dcore_user_get_access($uid) {
  global $user;
  return (($user->uid == $uid and user_access('get own user data')) or
    ($user->uid != $uid and user_access('get any user data')));
}

/**
 * Check if a user is logged.
 *
 * @param $uid
 *   Number. The user ID.
 */
function _wsservice_dcore_user_logged($uid) {
  return TRUE;
  return FALSE;
}

/**
 * Check if a user is logged.
 *
 * @param $uid
 *   Number. The user ID.
 */
function _wsservice_dcore_user_logged_access($uid) {
  global $user;
  return (($user->uid == $uid and user_access('check if the own user is logged')) or
    ($user->uid != $uid and user_access('check if the any user is logged')));
}

/**
 * Login a user
 *
 * @param $username
 *   String. The username.
 * @param $password
 *   String. The user password.
 */
function _wsservice_dcore_user_login($username, $password) {
  global $user;

  if ($user->uid) {
    // user is already logged in
    return webservices_error(t('Already logged in as !user.', array('!user' => $user->name)));
  }

  $user = user_authenticate(array('name' => $username, 'pass' => $password));

  if ($user->uid) {
    session_start();
    $return = new stdClass();
    $return->sessid = session_id();
    $return->user = $user;
    return $return;
  }
  session_destroy();
  return webservices_error(t('Wrong username or password.'));
}

/**
 * Logout user
 */
function _wsservice_dcore_user_logout($uid) {
  if (!$user_external = user_load($uid)) {
    return webservices_error('User doesnt exist.');
  }

  // "Impersonate" the user
  global $user;
  $user_backup = $user;
  session_save_session(FALSE);

  $user = $user_external;
  module_load_include('pages.inc', 'user');
  user_logout();

  // Restore the user data
  $user = $user_backup;
  session_save_session(TRUE);

  return TRUE;
}

/**
 * Save user details.
 *
 * @param $user_data
 *   Object. The user object with all user data.
 */
function _wsservice_dcore_user_save($account) {
  // If uid is present then update, otherwise insert
  $account = user_save((object)$account, $account);
  if (!$account) {
    return webservices_error(t('Error on saving the user.'));
  }

  // Everything went right.
  // Return the user ID
  return $account->uid;
}

/**
 * Check if the user is allowed to get the user data.
 *
 * @param $account
 *   Object. The user information.
 */
function _wsservice_dcore_user_save_access($account) {
  global $user;
  return ((empty($account->uid) and user_access('create new users')) or
    ($user->uid == $account->uid and user_access('update own user data')) or
    ($user->uid !=  $account->uid and user_access('update any user data')));
}
