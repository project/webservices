<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Link general node functionalities to services module.
 */

/**
 * Returns a specified node.
 *
 * @param $nid
 *   Number. The node ID.
 * @param $fields
 *   Array (optinonal). The node fields needed. If its empty,
 *   all fields will be returned
 * @return
 *   Object. The node object with all wanted fields.
 */
function _wsservice_dcore_node_get($nid, $fields = array()) {
  if (!$node = _wsservice_dcore_node_load(node_load($nid), $fields)) {
    return webservices_error(t('Could not find the node.'));
  }

  return $node;
}

/**
 * Check if the user has the permission to get the
 * node's data thru services.
 *
 * @param $nid
 *   Number. The node ID.
 * @return
 *   Boolean. TRUE if the user has the permission to get the
 *   node's data thru services.
 */
function _wsservice_dcore_node_get_access($nid) {
  global $user;
  $node = node_load($nid);
  return ($node->uid == $user->uid and user_access('load own node data from remote')) or
    ($node->uid != $user->uid and user_access('load any node data from remote'));
}

/**
 * Make any changes we might want to make to node.
 */
function _wsservice_dcore_node_load($node, $fields = array()) {
  if (!isset($node->nid)) {
    return NULL;
  }

  // Apply filters to fields
  $body = $node->body;
  $node->body = new stdClass();
  $node->body_value = $body;
  $node->body = check_markup($body, $node->format, FALSE);

  // Loop through and get only requested fields
  if (empty($fields)) {
    $val = $node;
  }
  else {
    foreach ($fields as $field) {
      $val->{$field} = $node->{$field};
    }
  }

  return $val;
}

/**
 * Save a node. It creates a new one, in case the 'nid' field
 * is missing.
 *
 * @param $node
 *   Array. The node fields' values, just like created on
 *   node edit form.
 * @return
 *   Number. The node ID.
 */
function _wsservice_dcore_node_save($node) {
  // Load the required includes for drupal_execute
  module_load_include('inc', 'node', 'node.pages');

  // Check if its updating an existing node
  if ($node['nid'] and !$node_existing = node_load($node['nid'])) {
    return webservices_error(t('Node not found'));
  }

  // Setup form_state
  $form_state = array();
  $form_state['values'] = (array) $node;
  $form_state['values']['op'] = t('Save');

  drupal_execute($node['type'] .'_node_form', $form_state, (object)$node);

  if ($errors = form_get_errors()) {
    return webservices_error(implode("\n", $errors));
  }

  watchdog('content', '@type: updated %title.',
    array('@type' => $form_state['node']['type'], '%title' => $node['node']['title']),
    WATCHDOG_NOTICE, l(t('view'), 'node/'. $form_state['nid']));
  return $form_state['nid'];
}

/**
 * Check if the user has the permission to save a node.
 *
 * @param $node
 *   Object. The node object.
 * @return
 *   Boolean. TRUE if the user has the permission to save a node.
 */
function _wsservice_dcore_node_save_access($node) {
  if (isset($node['nid'])) {
    return node_access('update', $node);
  }
  return node_access('create', $node['type']);
}

/**
 * Check if the user has the permission to delete a node.
 *
 * @param $nid
 *   Number. The node ID.
 * @return
 *   Boolean. TRUE if the user has the permission to delete a node.
 */
function _wsservice_dcore_node_delete_access($nid) {
  $node = node_load($nid);
  return node_access('delete', $node);
}
