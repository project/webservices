<?php

/**
 * @file
 * Hooks provided by Web Services.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Register a new service available for Web Services.
 *
 * New services can be provided for external applications (other sites, desktop
 * programms). You must register the basic services attributes:
 *   - "#method":           The service name
 *   - "#callback":         The function with will be called
 *   - "#access callback":  (optional) The function that will verify the if the
 *                          user has the permission to access it, in case an
 *                          special check is needed.
 *   - "#no_key":           (optional)
 *   - "#description":      A brief description about what this service does
 *   - "#args":             The list of arguments required by the service. It
 *                          needs a sublist containing:
 *                            - "#name": The variable name
 *                            - "#type": The  variable type (int, string, array...)
 *                            - "#description": A brief description about what
 *                              is this varialbe
 *   - "#return":           The type of returning value.
 *
 * @return
 *   Array. A list of services and their attributes.
 */
function hook_webservices_info() {
  return array(
    array(
      '#method'           => 'views.get',
      '#callback'         => '_wsservice_views_get',
      '#access callback'  => '_wsservice_views_get_access',
      '#args'             => array(
        array(
          '#name'           => 'view_name',
          '#type'           => 'string',
          '#description'    => t('View name.')
        ),
        array(
          '#name'           => 'fields',
          '#type'           => 'array',
          '#optional'       => TRUE,
          '#description'    => t('A list of fields to return.')
        ),
      ),
      '#return'           => 'array',
      '#description'      => t('Retrieves a view defined in views.module.')
    ),
  );
}

/**
 * Register a new type of authentication for Web Services.
 *
 * Web Services module can use different authentication methods.
 *
 * @return
 *   Array. The authentication method's attributes "#name" and "#class".
 *
 * This hook will only be called if cron.php is run (e.g. by crontab).
 */
function hook_webservices_auth_info() {
  return array(
    '#name'   => t('Direct User ID'),
    '#class'  => '_webservices_directuid_auth',
  );
}

/**
 * @} End of "addtogroup hooks".
 */
