<?php
/**
 * @file
 * Contains the base row style plugin.
 */

/**
 * The basic 'fields' row plugin
 *
 * This displays fields one after another, giving options for inline
 * or not.
 *
 * @ingroup views_row_plugins
 */
class wsservice_views_plugin_row_service_fields extends views_plugin_row {
  /**
   * Render a row object. This usually passes through to a theme template
   * of some form, but not always.
   */
  function render($row) {
    $output = array();
    foreach ($this->view->field as $id => $field) {
      $output[$id] = $field->render($row);
    }

    return $output;
  }
}


