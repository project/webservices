<?php
/**
 * @file
 * Contains the RSS style plugin.
 */

/**
 * Default style plugin to render an RSS feed.
 *
 * @ingroup views_style_plugins
 */
class wsservice_views_plugin_style_service extends views_plugin_style {
  function render() {
    $rows = array();

    /*
    // This will be filled in by the row plugin and is used later on in the
    // theming output.
    $this->namespaces = array();
    */

    /*
    $rows['theme_functions'] = $this->theme_functions();
    $rows['display_options'] = $this->display->handler->get_option('row_options');
    $rows['options'] = $this->options;
    */

    foreach ($this->view->result as $row) {
      $rows[] = $this->row_plugin->render($row);
    }

    // Services requires an array.
    return $rows;
  }
}
