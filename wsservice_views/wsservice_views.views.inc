<?php
/**
 * @file
 * All Views 2  plugins and handlers declaration.
 */

/**
 * Implementation of hook_views_plugins().
 */
function wsservice_views_views_plugins() {
  return array(
    'display' => array(
      'wsservice_views_feed' => array(
        'title' => t('Services Feed'),
        'help' => t('Send the results of this view to Services.'),
        'handler' => 'wsservice_views_plugin_display_services_feed',
//        'parent' => 'page', // so it knows to load the page plugin .inc file
        'uses hook menu' => FALSE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'admin' => t('Services Feed'),
        'help topic' => 'display-services-feed',
      ),
    ),
    'style' => array(
      'wsservice_views_style' => array(
        'title' => t('Service'),
        'help' => t('Sends the results to Services.'),
        'handler' => 'wsservice_views_plugin_style_service',
//        'theme' => 'views_view',
        'type' => 'wsservice_views_feed', // only shows up as a summary style
        'uses fields' => TRUE,
        'uses options' => FALSE,
        'uses row plugin' => TRUE,
        'help topic' => 'style-service',
      ),
    ),
    'row' => array(
      'wsservice_views_fields' => array(
        'title' => t('Services Fields'),
        'help' => t('Displays the fields for output to Services with an optional template.'),
        'handler' => 'wsservice_views_plugin_row_service_fields',
        'theme' => 'views_view_fields',
        'uses fields' => TRUE,
        'uses options' => FALSE,
        'type' => 'wsservice_views_feed',
        'help topic' => 'style-services-row-fields',
      ),
      'wsservice_views_nodes' => array(
        'title' => t('Services Nodes'),
        'help' => t('Displays nodes for output to Services with an optional template.'),
        'handler' => 'wsservice_views_plugin_row_service_nodes',
//        'theme' => 'views_view_fields',
        'uses fields' => FALSE,
        'uses options' => FALSE,
        'type' => 'wsservice_views_feed',
        'help topic' => 'style-services-row-nodes',
      ),
      'wsservice_views_raw' => array(
        'title' => t('Services Raw'),
        'help' => t('Sends raw DB data to Services.'),
        'handler' => 'wsservice_views_plugin_row_service_raw',
//        'theme' => 'views_view_fields',
        'uses fields' => TRUE,
        'uses options' => FALSE,
        'type' => 'wsservice_views_feed',
        'help topic' => 'style-services-row-nodes',
      ),
    ),
  );
}

