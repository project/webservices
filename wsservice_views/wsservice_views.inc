<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Link general views functionalities to services module.
 */

/**
 * Get a view from the database.
 *
 * @param $view_name
 *   String. The views name.
 * @param $display_id
 *   String (optional).
 * @param $args
 *   Array (optional). A list of params.
 * @param $display_id
 *   String (optional). The display ID.
 * @return
 *   Array. The views return.
 */
function _wsservice_views_get($view_name, $fields = array(), $args = array(), $offset = 0, $limit = 0, $display_id = 'default') {
  $view = views_get_view($view_name);

  // Put all arguments and then execute
  //$view->set_arguments($args, FALSE);
  $view->set_offset($offset);
  $view->set_items_per_page($limit);

  if ($display_id == 'default') {
    foreach ($view->display as $id => $display) {
      if ($display->display_plugin == 'wsservice_views_feed') {
        $display_id = $id;
        break;
      }
    }
  }

  return $view->execute_display($display_id, $args);
}

/**
 * Check the access permission to a given views.
 *
 * @param view_name
 *   String. The views name.
 * @return
 *   Boolean. TRUE if the user is allowed to load the given view.
 */
function _wsservice_views_get_access($view_name) {
  $view = views_get_view($view_name);
  if (empty($view)) {
    return FALSE;
  }

  global $user;
  return views_access($view);
}

/**
 * Export a view.
 *
 * @param view_name
 *   String. The views name.
 * @return
 *   Array. The view object.
 */
function _wsservice_views_export($view_name) {
  $view = views_get_view($view_name);
  if (empty($view)) {
    return webservices_error('View does not exist.');
  }

  return $view->export();
}

/**
 * Import a view.
 *
 * @param $view_import
 *   Array. The view object.
 * @param $view_name
 *   String (optional). The view name.
 * @return
 *   Number. The new view ID
 */
function _wsservice_views_import($view_import, $view_name = '') {
  // Include the necessary files
  require_once drupal_get_path('module', 'views') .'/includes/admin.inc';

  // Import the the views using the same form as in-site import
  $form_state['values']['name'] = $view_name;
  $form_state['values']['view'] = $view_import;
  $form_state['values']['op'] = t('Import');
  drupal_execute('views_ui_import_page', $form_state);

  // Check if there is a any error
  if ($errors = form_set_error()) {
    return webservices_error($errors);
  }

  // At this point, the new view was only cached and now its time
  // to save it and return the new View ID
  $view = $form_state['view'];
  $view->save();
  return $view->vid;
}
