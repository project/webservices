<?php
/**
 * @file
 * Contains the feed display plugin.
 */

/**
 * The plugin that handles a feed, such as RSS or atom.
 *
 * For the most part, feeds are page displays but with some subtle differences.
 *
 * @ingroup views_display_plugins
 */
class wsservice_views_plugin_display_services_feed extends views_plugin_display {
  function uses_breadcrumb() { return FALSE; }
  function get_style_type() { return 'wsservice_views_feed'; }

  /**
   * Feeds do not go through the normal page theming mechanism. Instead, they
   * go through their own little theme function and then return NULL so that
   * Drupal believes that the page has already rendered itself...which it has.
   */
  function execute() {
    return $this->view->render($this->display->id);
  }

  function preview() {
    $result = $this->view->render($this->display->id);

    // HINT: This should always be true.
    if (is_array($result)) {
      $result = print_r($result, TRUE);
    }

    return '<pre>'. check_plain($result) .'</pre>';
  }

  /**
   * Instead of going through the standard views_view.tpl.php, delegate this
   * to the style handler.
   */
  function render() {
    return $this->view->style_plugin->render($this->view->result);
  }

  function defaultable_sections($section = NULL) {
    if (in_array($section, array('style_options', 'style_plugin', 'row_options', 'row_plugin'))) {
      return FALSE;
    }

    $sections = parent::defaultable_sections($section);

    return $sections;
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['displays'] = array('default' => array());

    // Overrides for standard stuff:
    $options['style_plugin']['default'] = 'wsservice_views_style';
    $options['style_options']['default']  = array('raw_data' => FALSE);
    $options['sitename_title']['default'] = FALSE;
    $options['row_plugin']['default'] = 'wsservice_views_raw';
    $options['defaults']['default']['style_plugin'] = FALSE;
    $options['defaults']['default']['style_options'] = FALSE;
    $options['defaults']['default']['row_plugin'] = FALSE;
    $options['defaults']['default']['row_options'] = FALSE;

    return $options;
  }
}
