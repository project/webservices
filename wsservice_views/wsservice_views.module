<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Link general views functionalities to services module.
 */

/**
 * Implementation of hook_webservices_info().
 */
function wsservice_views_webservices_info() {
  return array(
    array(
      '#method'           => 'views.get',
      '#callback'         => '_wsservice_views_get',
      '#access callback'  => '_wsservice_views_get_access',
      '#file'             => array('file' => 'inc', 'module' => 'wsservice_views'),
      '#no_key'           => TRUE,
      '#args'             => array(
        array(
          '#name'           => 'view_name',
          '#type'           => 'string',
          '#description'    => t('View name.')
        ),
        array(
          '#name'           => 'fields',
          '#type'           => 'array',
          '#optional'       => TRUE,
          '#description'    => t('A list of fields to return.')
        ),
        array(
          '#name'           => 'args',
          '#type'           => 'array',
          '#optional'       => TRUE,
          '#description'    => t('An array of arguments to pass to the view.')
        ),
        array(
          '#name'           => 'offset',
          '#type'           => 'int',
          '#optional'       => TRUE,
          '#description'    => t('An offset integer for paging.')
        ),
        array(
          '#name'           => 'limit',
          '#type'           => 'int',
          '#optional'       => TRUE,
          '#description'    => t('A limit integer for paging.')
        ),
        array(
          '#name'           => 'display_id',
          '#type'           => 'string',
          '#optional'       => TRUE,
          '#description'    => t('The Views2 display_id to use. If none is provided, the first Services Feed display for the given view will be used.'),
        ),
      ),
      '#return'           => 'array',
      '#description'      => t('Retrieves a view defined in views.module.')
    ),
    array(
      '#method'           => 'views.export',
      '#callback'         => '_wsservice_views_export',
      '#access arguments' => array('administer views'),
      '#file'             => array('file' => 'inc', 'module' => 'wsservice_views'),
      '#args'             => array('string'),
      '#args'             => array(
        array(
          '#name'           => 'view_name',
          '#type'           => 'string',
          '#description'    => t('View name.'),
        ),
      ),

      '#return'           => 'string',
      '#description'      => t('Exports the code of a view, same as the output you would get from the Export tab.'),
    ),
    array(
      '#method'           => 'views.import',
      '#callback'         => '_wsservice_views_import',
      '#access arguments' => array('administer views'),
      '#file'             => array('file' => 'inc', 'module' => 'wsservice_views'),
      '#args'             => array(
        array(
          '#name'           => 'view_import',
          '#type'           => 'string',
          '#size'           => 'big',
          '#description'    => t('Code from a Views->Export.'),
        ),
        array(
          '#name'           => 'view_name',
          '#type'           => 'string',
          '#optional'       => TRUE,
          '#description'    => t('The new Views name.')
        ),
      ),
      '#return'           => 'int',
      '#description'      => t('Imports a view through code, equivalent to using the Import tab in the views admin.'),
    ),
  );
}

/**
 * Implementation of hook_views_api().
 */
function wsservice_views_views_api() {
  return array(
    'api' => '2',
    'path' => drupal_get_path('module', 'wsservice_views'),
  );
}
