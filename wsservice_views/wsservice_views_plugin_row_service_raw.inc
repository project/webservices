<?php
/**
 * @file
 * Contains the base row style plugin.
 */

/**
 * Default plugin to view a single row of a table. This is really just a wrapper around
 * a theme function.
 *
 * @ingroup views_row_plugins
 */
class wsservice_views_plugin_row_service_raw extends views_plugin_row {
  /**
   * Render a row object. This usually passes through to a theme template
   * of some form, but not always.
   */
  function render($row) {
    return (array)$row;
  }
}
