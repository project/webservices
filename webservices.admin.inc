<?php
/**
 * @author Web Services Dev Team
 * @file
 *  Browser thru all services and servers.
 */

/**
 * List all services available.
 */
function _webservices_admin_service_list() {
  module_load_include('inc', 'webservices');
  $methods = webservices_service_get_all();

  if (empty($methods)) {
    return t('No services have been enabled.');
  }
  else {
    // Group namespaces
    $services = array();
    foreach ($methods as $method) {
      $namespace = drupal_substr($method['#method'], 0, strrpos($method['#method'], '.'));
      $services[$namespace][$method['#method']] = $method;
    }
    ksort($services);

    foreach ($services as $namespace => $methods) {
      ksort($methods);
      $output .= '<h2>' . $namespace . '</h2>';
      $output .= '<ul>';
      foreach ($methods as $method) {
        $output .= '<li class="leaf">' . l($method['#method'], 'admin/build/webservices/' . $method['#method']) . '</li>';
      }
      $output .= '</ul>';
    }

    return $output;
  }
}

/**
 * Allow users to test a given service.
 *
 * @param $method
 *   Object. The service info.
 *
 * @ingroup form
 */
function _webservices_admin_service_test($form_state, $service) {
  // Testing fields
  $form['arg'] = array(
    '#tree'   => TRUE,
  );
  foreach ($service['#args'] as $key => $arg) {
    $form['arg'][$key] = array(
      '#description'  => $arg['#description'],
      '#required'     => !$arg['#optional'],
      '#title'        => $arg['#name'],
      '#type'         => 'textfield',
    );
    if ($arg['#size'] == 'big') {
      $form['arg'][$key]['#type'] = 'textarea';
    }
  }

  $form['service'] = array(
    '#value'          => $service,
    '#type'           => 'value',
  );
  $form['service_name'] = array(
    '#value'          => $service['#method'],
    '#type'           => 'hidden',
  );

  $form['submit'] = array(
    '#ahah' => array(
      'path'      => 'admin/build/webservices/js',
      'wrapper'   => 'webservices-test-result',
      'progress'  => array('type' => 'bar', 'message' => t('Please wait...')),
    ),
    '#suffix'         => '<div id="webservices-test-result"></div>',
    '#type'           => 'submit',
    '#value'          => t('Test service')
  );
  return $form;
}

/**
 * AJAX callback to return a service test.
 *
 * @return
 *   Prints the services result in JSON format.
 */
function _webservices_admin_service_test_js() {
  $service  = $_POST['service_name'];
  $args     = $_POST['arg'];
  module_load_include('inc', 'webservices');
  $result = _webservices_service_call($service, $args);
  drupal_json(array('status' => TRUE, 'data' => print_r($result, TRUE)));
  exit();
}

/**
 * Configure the Services module
 */
function _webservices_admin_settings() {
  $form['security'] = array(
    '#description'  => t('Changing security settings will require you to adjust all method calls. This will affect all applications using site services.'),
    '#title'        => t('Security'),
    '#type'         => 'fieldset',
  );
  foreach (module_implements('webservices_auth_info') as $module) {
    $method = module_invoke($module, 'webservices_auth_info');
    $auth_methods[$module] = $method['#name'];
  }
  $form['security']['webservices_auth'] = array(
    '#default_value'  => variable_get('webservices_auth', 'webservices'),
    '#description'    => t('Choose carefully what authentication method your webservices will require. It the main security measure.'),
    '#options'        => $auth_methods,
    '#title'          => t('Authentication method'),
    '#type'           => 'radios',
  );
  return system_settings_form($form);
}
