
DESCRIPTION
===========
- Create a unified Drupal API for web services to be exposed in a variety of
  different server formats.
- Provide a service browser to be able to test services.
- Use OAuth authentication for security.


INSTALL
=======
- Install the main module, "Web Services"
- Install the wanted server module: XML-RPC, JSON, SOAP, etc.
- Install an authentication method, like OAuth (recommended).
    A generic and simple authentication method is provided with "Web Services",
    but its absolutely unsecure. Its only good during development of a new
    service.
- Install the wanted services. Both Drupal Core and Views services are
    provided out of the box.
- Select on admin/settings/webservices what authentication method will be used.
- Select which user role will be able to "access services" (as a user) and which
    will be able to "integrate external application" (as a consumer).
- Also, select the proper persimssions to each service for each role. Example:
    "editors" role might "get any node data" while regular "authenticated" users
    might only "get own node data".


SERVICES
========
Web Services are some methods that an external site can call. External sites might
  publish new content, change user details, upload a new image, etc.
The most common "consumer" of web services are other sites. A site that print photos
  might integrate with popular photo sites to automatically download the user
  photos. But there are other types of "consumers": flash programs, desktop programs,
  and widgets are also possible consumers. Users might let them gather data from
  your site in their behalf.


AUTHENTICATION
==============
Some web services are very powerful and might alter important data from your site.
  Thats why you should select carefully what users have the permission
  to access certain services and what services they are forbidden to access.
Sites can easily check if a user is a anonnymous or registered when they are
  actually visisting the site: they can check the cookies and session values.
  But its not that easy when the user is remote.
When a remote user says "Hey site, I want the information about the node 34", the
  site cannot know for sure if the user is allowed to get such information.
The situation is even more complicated when another site says "Hey site, a user
  from my site wants to get node 34". Now i need to find out 2 things: if hte user
  is allowed to get the node data and if this site is allowed to get it IN BEHALF
  of the user.
So here we need authentication methods for remote users. There are several techniques
  to make sure that not only that user is the one who he claims to be but if he
  really allowed the site to get this information for him.
Choosing the right authentication solution is vital, because otherwise your site
  will be exposed to a SERIOUS security problem.
Web Services module might use the OAuth. Its a international method, created by 2
   employees from Google and Twitter. It adds a very complete security way to let
   you open your internal APIs. But its wont be that easy for your users to implement,
   so you will need to make them read the OAuth specifications in order to
   successfully integrate their sites with yours.


SECURITY ON COMMUNICATION
=========================
Even good authentication methods, like OAuth, will only ensure that the users is
  the one he is claiming to be. However, its like screamming a personal secret to
  another person in the other side of a full room: everyone there might listen
  your secret. So you also need a way to communicate with users in a safe manner.
The most common way to do so is thru HTTPS. Try to get more information on the
  internet about this protocol and check if your host provider have this feature.


DEVELOPMENT
===========
The Web Services module is a strong API that allow you to create more services,
  servers and authentication methods. Go to the module's page and check the latest
  documentation about it.
